## Библиотека прямого действия

*Этой библиотеке еще предстоит наполниться контентом.*

Данный проект представляет собой библиотеку со свободным доступом, содержащую в себе материалы, которые могут быть полезны для различных видов активизма. Целью проекта является агрегация актуальной информации и публикация материалов, приближенных к формату инструкций, решающих конкретные задачи. Большинство тезисов, которые приводятся в статьях, носит обязательный характер и если вы не очень хорошо разбираетесь в определенной теме, лучшим вариантом будет полностью следовать тому, что написано в статье - так мы преподносим материал.

В отличие от данного проекта, на других платформах со схожей тематикой размещается разрозненная информация, которая зачастую публикуется вперемешку с новостями или идеологической литературой.

Любой желающий может самостоятельно сделать запрос на добавление материала в библиотеку, который будет рассмотрен нами.

* Gitlab: [https://gitlab.com/revlib/revlib.gitlab.io/tree/master/source](https://gitlab.com/revlib/revlib.gitlab.io/tree/master/source)
* E-mail: [nnLgDpMnK4gRzG6bDkAvhCXUd6ftHuZvk@protonmail.com](mailto:nnLgDpMnK4gRzG6bDkAvhCXUd6ftHuZvk@protonmail.com)

Рекомендуется использовать данный  ключ для отправки зашифрованных email сообщений:
```
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: OpenPGP.js v3.1.3
Comment: https://openpgpjs.org

xsBNBFvmB+oBCADh0Bdx4bngjo9SWpnV8hmnzB2GS2bZutAIeiOZUlfuEX6p
UcapxL2eLxX/UC/OPCjYbBhdDU6Vvudw695A9CfzaYZULOqs8Tsks7WEmAYf
LVosAPWhpfyqgymfyV1dqREPPP4JydfNQ2kyheZBvGldsa0pcVhr7IrkHMIb
S11JcR/TTkStEx0aVe801zR/y8jKpKgN6ayEcrLTBQitq0Ug0TnEcE3pMRqz
l9MKwc/QLQPUwpTFONMuRojRumKWycWb0ub79w3u9SD1ZwlIHD7cJGFBEYMw
FLf1z3fsn5AZ0r/uelPmsEcLUgO2eTnRT/aFvyXVrx6iMXRAW4Z82RSLABEB
AAHNZSJubkxnRHBNbks0Z1J6RzZiRGtBdmhDWFVkNmZ0SHVadmtAcHJvdG9u
bWFpbC5jb20iIDxubkxnRHBNbks0Z1J6RzZiRGtBdmhDWFVkNmZ0SHVadmtA
cHJvdG9ubWFpbC5jb20+wsB1BBABCAApBQJb5gfqBgsJBwgDAgkQXt2zS2oj
W58EFQgKAgMWAgECGQECGwMCHgEAAFsyB/9aTlC1VHo0PjFgVZDQxQItXAJV
pH0AoaytOsvbrft0X3yoeCjFZR8f0LNb3Qg2emShSeopdOP6dYIT5b8leLTt
NmX2c5qBsgspqEjWt5q8SIPXekxwtKCEwqPRBTW9L3Fe6VMa/oEe7Lm9xqlF
0sR80dVNQGK+KsJuLqnVDRT2NiQ9AmxcQoTydYtYYN+NnkVy9V0MLyslA07v
aolTQ9Suh/qculgPesybmk81w0YokuGpU9uV3u71cnhXaJCQX0LMsCmMqTav
vvleRzGBifaKqV+zfJRYtJHGlWdp18/4lA1BuI/zAnTlSG6aHeoL6iW162MW
x5vnFvYbzsIspda4zsBNBFvmB+oBCAC7jto4jGFKgs8aLun1jfoKssZk/wze
vGko5tbEqwVcsAAgQPKLLq9Ii5dMf6SKho9cw7oKzKxxuMKSgh2f/Qs40Hw5
tzP9SD2dgTyGy1a1AP2seexUuZDqNsK2QlGFC2sH5kItPNp87A2A9tlrjdvQ
H9evHUo8y4Liv4NlIN4mAQLdI0XpyzwM1wnBYn/pn796/zYoRza0DgZ4d7QM
PT/xbQZzR67hAimbPTEYtKMg7KacQkD5PnNN15nQS1OuMytiOqY3JZjvREsB
V6iZ+FxSA08uzvUFO6QrQLbY9DbZ0HkOWMqmlx5n6j50qBcQQLh/Pw2WmvFT
0HllrrS6Uu3rABEBAAHCwF8EGAEIABMFAlvmB+oJEF7ds0tqI1ufAhsMAAB3
+Qf/fH0oaoYWY8iIpdS8rRSqNKj9elfcXwN0V5t8j6mBQpMvod0qjpTj6n4I
sI28TPHDKYNjT+TpO6RGSgRL0RYqbkDp4+u4qM8dfyTnLwkBocUDEZ7vUXi4
uqzeXg3FVEfsuaf0Cs278XbZBgHc1mSp+baMKHvNMRtz3xQQuOZc8J8ribtW
bWDvfn35Sw2EHCCGPp2+p6FOdTyErZgDuAqpGqrBINAxJz1tktVZSzgNVv53
8hQ8VQVDEC089VOuc0pNhyLOuS6/GlHIkaMkVaClkpz3r77GMb4HVhLfcBPN
7rSf9beZxRsoefoQ9syMxrdoHuLQMkvOEF0bQNDiALwI6w==
=UM0Y
-----END PGP PUBLIC KEY BLOCK-----
```

Некоторые тексты, представленные здесь, могут быть скопированы с других тематических ресурсов с указанием первоисточника. 

Содержимое доступно по лицензии [WTFPL public license](?p=LICENSE).