var _e = function (id) { return document.getElementById(id); };
var xhrErrorOccured = false;

function xhrSuccess() { 
    this.callback.apply(this, this.arguments); 
}

function xhrError() { 
    console.error('XHR request error');
    if (!xhrErrorOccured) {
        xhrErrorOccured = true;
        loadFile('content/404.md', loadMD);
    }
}

function loadFile(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.callback = callback;
    xhr.arguments = Array.prototype.slice.call(arguments, 2);
    xhr.onload = xhrSuccess;
    xhr.onerror = xhrError;
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
    xhr.overrideMimeType("text/plain");
    xhr.send(null);
}

function loadMD() {
    badword = "The page you're looking for could not be found (404)";
    response = this.responseText;
    var md = window.markdownit();
    if (response.indexOf(badword) == -1) {
        _e("markdown-body").innerHTML = md.render(response);
        _e("content-box").scrollTop = 0;
    } else {
        loadFile('content/404.md', loadMD);
    }
}

function updateMDPath(path) {
    if (history.pushState) {
        var newurl = null
        if (path != null) {
            path = path.replace('.md', '');
            newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?p=' + path;
        } else {
            newurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
        }
        window.history.pushState({path:newurl},'',newurl);
    }
}

function setNavigationContent() {
    var data = this.responseText;
    var paths = [];
    breaklinePos = -1
    for (var i = 1; i < data.length; i++) {
        if (data[i] == '\n') {
            paths.push(data.substring(breaklinePos+1, i));
            breaklinePos = i;
        }
    }

    var makeul = function(hierarchy){
        var dirs = Object.keys(hierarchy);
        var ul = '<ul class="nav-elem">';
        dirs.forEach(function(dir) {
            var path = hierarchy[dir].path;
            if (path) { // file
                dir = dir.replace('.md', '');
                var hash = sha1(path);
                ul += '<li class="item nav-elem file" id="'+hash+'" onclick="loadFile(\'structure.txt\', go, this.id)">'+dir+'</li>\n';
            } else {
                ul += '<li class="item nav-elem folder">'+dir+'\n';
                ul += makeul(hierarchy[dir]);
                ul += '</li>\n';
            }
        });
        ul += '</ul>\n';
        return ul;
    };

    var hierarchy = paths.reduce(function(hier, path) {
        var x = hier;
        path.split('/').forEach(function(item) {
            if (!x[item]) {
                x[item] = {};
            }
            x = x[item];
        });
        x.path = path;
        return hier;
    }, {});

    var content = '<nav class="vertical">' + makeul(hierarchy) + '</nav>';
    _e("navigation-box").innerHTML = content;
}

function go(hash) {
    var structure = this.responseText;
    var record = null;
    breaklinePos = -1
    for (var i = 1; i < structure.length; i++) {
        if (structure[i] == '\n') {
            text = structure.substring(breaklinePos+1, i)
            breaklinePos = i;
            if (text.indexOf(hash) != -1) {
                record = text;
            }
        }
    }
    
    var path = record.substring(record.indexOf('|')+1, record.length);
    if (path.indexOf('.md') != -1) {
        updateMDPath(path);
        loadFile('content/'+path, loadMD);
    } else {
        var win = window.open('content/'+path, '_blank');
        win.focus();
    }
}
